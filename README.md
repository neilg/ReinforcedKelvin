## Reinforced Kelvin lattice with 2D parts

<img src=Parametric.png width="100%"><br>

<img src=Lattice.png width="100%"><br>

<img src=Lattice.jpg width="100%"><br>

<img src=CornersEdges.png width="100%"><br>

<img src=CornersEdges.jpg width="100%"><br>

<img src=ReinforcedKelvin.jpg width="100%"><br>

<img src=Clips.jpg width="100%"><br>
